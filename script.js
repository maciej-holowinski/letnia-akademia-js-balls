function game() {
  const lowerRange = document.querySelector(".range-lower");
  const upperRange = document.querySelector(".range-upper");
  const gameBoard = document.querySelector(".game-board");
  const startButton = document.querySelector(".start-button");
  const addButton = document.querySelector(".add-button");
  const accuracySpan = document.querySelector(".accurracy");
  let gameBoardSize;
  let ballCount, hitBallCount;

  /*OBSERWATOR ZDARZEŃ*/

  window.addEventListener("keyup", e => {
    e.preventDefault();
  });

  //tworzy obserwator dla przycisku start

  startButton.addEventListener("click", play);

  //tworzy obserwator dla przycisku add

  let buttonListener = createBall.bind(null, false, false);
  addButton.addEventListener("click", buttonListener);

  //tworzy obserwator dla spacji

  let windowListener = function (event) {
    if (event.keyCode === 32) {
      createBall.call(null, false, false);
    }
  };
  window.addEventListener("keyup", windowListener);

  /*GŁÓWNA FUNKCJA*/

  function play() {
    ballCount = 0;
    hitBallCount = 0;
    gameBoardSize = parseInt(
      window.getComputedStyle(gameBoard).width.slice(0, -3)
    );
    //pobiera zakres
    let min = parseInt(lowerRange.value);
    let max = parseInt(upperRange.value);
    //losuje liczbę piłek
    let ballsNumber = getRandom(min, max);

    let lastBall = false;
    let delay = 0;

    for (let i = 0; i < ballsNumber; i++) {
      let random = getRandom(300, 700);
      delay = delay + random;

      if (i == ballsNumber - 1) {
        lastBall = true;
      }
      setTimeout(createBall.bind(null, true, lastBall), delay);
    }
  }

  /*TWORZENIE PIŁEK*/

  function createBall(autoCreate, lastBall) {
    ballCount++;
    console.log(ballCount);
    //ustala kolor piłki w oparciu o sposb jej utworzenia
    let ballType = autoCreate ? getRandom(1, 4) : 5;
    //losuje położenie startowe
    let randomPlacement = getRandom(0, gameBoardSize - 60);
    //tworzy element
    let newElement = document.createElement("div");
    newElement.style.left = randomPlacement + "px";
    newElement.className = `ball ball-${ballType}`;

    //przypisuję setTimeout ID do zmiennej
    let finish = setTimeout(removeBall.bind(null, newElement, false), 5000);

    //tworzy zdarzenie
    newElement.addEventListener("click", e => {
      e.target.className += " sadBall";
      updateAccuracyUI();
      setTimeout(removeBall.bind(null, newElement, true), 1000);
      clearTimeout(finish);
    });

    //dodaje element do planszy
    gameBoard.appendChild(newElement);

    //usuwam obserwatory jeżeli ostatnia piłka z zakresu
    if (lastBall) {
      addButton.removeEventListener("click", buttonListener);
      window.removeEventListener("keyup", windowListener);
    }

    updateAccuracyUI();
  }

  function removeBall(element, wasHit) {
    if (wasHit) {
      hitBallCount++;
    }
    element.parentNode.removeChild(element);
    if (hasCompleted()) {
      alert('do you want to continue? TODO')
    }
  }

  function hasCompleted() {
    return !gameBoard.children.length;
  }

  function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function calculateAccuracy() {
    return (hitBallCount / ballCount * 100).toFixed(2) + "%"
  }

  function updateAccuracyUI() {
    accuracySpan.innerText = calculateAccuracy()
  }
}

game();